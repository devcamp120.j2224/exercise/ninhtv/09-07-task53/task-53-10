package com.devcamp.j04_javabasic.s10;

public class CPet extends CAnimal {
	int age;
	String name;
	@Override
	public void animalSound() {
		// TODO Auto-generated method stub
		System.out.println("Pet sound...");
	}
	@Override
	public void eat() {
		System.out.println("Pet eating...");
	};
	protected void print() {	
	}
	protected void  play() {	
	}
	public static void main(String[] args) {
		 
		CBird myBird = new CBird();
		myBird.name = "My Eagle";
		myBird.animclass = AnimalClass.birds;
		myBird.eat();
		myBird.animalSound();
		myBird.print();
		myBird.play();
		myBird.fly();

		CPet myFish = new CFish();
		myFish.name = "Gold Fish";
		myFish.animclass = AnimalClass.fish;
		myFish.eat();
		myFish.animalSound();
		myFish.print();
		myFish.play();
		((CFish) myFish).swim();

		CPet myPet = new CPet();
		myPet.name = "KIYA";
		myPet.age = 2;
		myPet.animclass = AnimalClass.mammals;
		myPet.eat();
		myPet.animalSound();
		myPet.print();
		myPet.play();
	}
}
