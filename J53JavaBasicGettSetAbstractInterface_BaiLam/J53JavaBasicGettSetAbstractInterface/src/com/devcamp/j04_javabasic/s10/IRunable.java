package com.devcamp.j04_javabasic.s10;

public interface IRunable extends IBarkable {
	void run() ;
	void running() ;
}
